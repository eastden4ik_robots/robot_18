package su.sviridoff.robots.tasks;

import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Board;
import com.julienvey.trello.domain.TList;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import su.sviridoff.robots.config.TrelloListsConfig;
import su.sviridoff.robots.models.TrelloCard;
import su.sviridoff.robots.providers.SettingsProvider;
import su.sviridoff.robots.utils.TelegramService;
import su.sviridoff.robots.utils.Utils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;


@Slf4j
@AllArgsConstructor
public class FetchCardsTask implements Runnable {

    private Trello trello;
    private String boardName;
    private TelegramService telegramService;
    private TrelloListsConfig trelloListsConfig;
    private SettingsProvider provider;

    public void run() {
        Map<String, List<TrelloCard>> lists = new HashMap<>();

        Board board = trello.getBoard(boardName);
        log.info("Receive all card from boards.");
        log.debug("Board: {}, Lists amount is {}", board.getName(), board.fetchLists().size());


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(provider.getTimeZone()));
        telegramService.send(String.format("[%s]:   Задачи с доски   %s   ", sdf.format(new Date()), board.getName()));

        board.fetchCards().forEach(card -> {
            Optional<TList> listName = board.fetchLists()
                    .stream()
                    .filter(tList -> tList.getId().equalsIgnoreCase(card.getIdList()))
                    .findFirst();
            if (listName.isPresent()) {

                if (lists.get(listName.get().getName()) != null) {
                    TrelloCard trelloCard = Utils.setTrelloCard(card);
                    lists.get(listName.get().getName()).add(trelloCard);
                    lists.put(listName.get().getName(), lists.get(listName.get().getName()));
                } else {
                    TrelloCard trelloCard = Utils.setTrelloCard(card);
                    List<TrelloCard> tmp = new ArrayList<>();
                    tmp.add(trelloCard);
                    lists.put(listName.get().getName(), tmp);
                }
            }

        });

        trelloListsConfig.getLists().forEach((key, value) -> {
            if (lists.get(key) != null) {
                String tasks = value + Utils.setMessage(lists.get(key));
                log.debug(tasks);
                telegramService.send(tasks);
            }
        });

        log.info("All cards from Trello sent through Telegram.");

    }

}
