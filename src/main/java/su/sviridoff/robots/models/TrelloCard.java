package su.sviridoff.robots.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
public class TrelloCard {

    private String name;
    private List<String> labels;

}
