package su.sviridoff.robots.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class TelegramMessage {

    private String chat_id;
    private String text;

}
