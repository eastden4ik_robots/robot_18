package su.sviridoff.robots.jobs;

import com.julienvey.trello.Trello;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import su.sviridoff.robots.config.CronConfig;
import su.sviridoff.robots.config.TrelloBoardsConfig;
import su.sviridoff.robots.config.TrelloListsConfig;
import su.sviridoff.robots.providers.SettingsProvider;
import su.sviridoff.robots.tasks.FetchCardsTask;
import su.sviridoff.robots.utils.TelegramService;

import java.util.TimeZone;


@Slf4j
@Component
@EnableScheduling
@EnableAsync
@AllArgsConstructor
public class ScheduledJob {

    private TaskScheduler taskScheduler;
    private CronConfig cronConfig;
    private TrelloBoardsConfig trelloBoardsConfig;
    private TrelloListsConfig trelloListsConfig;
    private Trello trello;
    private TelegramService telegramService;
    private SettingsProvider provider;

    @Bean
    public void scheduleAllCronForJob() {
        log.info("Execute - scheduleAllCronForJob");
        cronConfig.getSchedules().forEach(cron ->
                trelloBoardsConfig.boards().forEach((key, val) -> {
                    log.debug("Cron: {} for {}:{}", cron, key, val);
                    taskScheduler.schedule(new FetchCardsTask(trello, val, telegramService, trelloListsConfig, provider), new CronTrigger(cron, TimeZone.getTimeZone(provider.getTimeZone())));
                }));


    }

}
