package su.sviridoff.robots.providers;


import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class SettingsProvider {

    @Value("${timezone}")
    private String timeZone;

    @Value("${trello.key}")
    private String trelloKey;
    @Value("${trello.token}")
    private String trelloToken;
    @Value("${trello.url}")
    private String trelloUrl;

    @Value("${telegram.url}")
    private String tgURL;
    @Value("${telegram.token}")
    private String tgToken;
    @Value("${telegram.chat_id}")
    private String tgChatId;



}
