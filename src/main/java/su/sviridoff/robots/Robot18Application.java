package su.sviridoff.robots;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Robot18Application {

	public static void main(String[] args) {
		SpringApplication.run(Robot18Application.class, args);
	}

}
