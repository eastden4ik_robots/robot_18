package su.sviridoff.robots.utils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import su.sviridoff.robots.models.TelegramMessage;
import su.sviridoff.robots.providers.SettingsProvider;

@Slf4j
@Component
@AllArgsConstructor
public class TelegramService {

    private SettingsProvider provider;

    @SneakyThrows
    public void send(String message) {
        TelegramMessage tgMessage = new TelegramMessage();

        tgMessage.setChat_id(provider.getTgChatId());
        tgMessage.setText(message);

        log.debug(provider.getTgURL() + provider.getTgToken() + "/sendMessage");
        log.debug(tgMessage.toString());
        HttpResponse<String> response = Unirest.post(provider.getTgURL() + provider.getTgToken() + "/sendMessage")
                .queryString("chat_id", tgMessage.getChat_id())
                .queryString("text", tgMessage.getText())
                .asString();
        if (response.getStatus() != 200)
            log.error("{} - {}", response.getStatus(), response.getBody());
        else
            log.debug("{} - {}", response.getStatus(), response.getBody());
        log.info("Message sent through Telegram");

    }

}
