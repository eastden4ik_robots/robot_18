package su.sviridoff.robots.utils;

import com.julienvey.trello.domain.Card;
import su.sviridoff.robots.models.TrelloCard;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Utils {

    public static TrelloCard setTrelloCard(Card card) {
        TrelloCard trelloCard = new TrelloCard();
        trelloCard.setName(card.getName());
        trelloCard.setLabels(new ArrayList<>());
        card.getLabels().forEach(label -> trelloCard.getLabels().add(label.getName()));
        return trelloCard;
    }

    public static String setMessage(List<TrelloCard> list) {
        AtomicReference<String> tmpStr = new AtomicReference<>(""); AtomicInteger i = new AtomicInteger(1);
        list.forEach(task -> {
            AtomicReference<String> tmpLabelStr = new AtomicReference<>("");
            task.getLabels().forEach(label -> tmpLabelStr.set(String.format("%s#%s ", tmpLabelStr.get(), label)));
            tmpStr.set(String.format("%s%d. %s %s\n",
                    tmpStr.get(),
                    i.get(),
                    task.getName(),
                    !tmpLabelStr.get().equals("")?
                    String.format("- %s", tmpLabelStr.get()): ""));
            i.getAndIncrement();
        });
        return tmpStr.get();
    }


}
