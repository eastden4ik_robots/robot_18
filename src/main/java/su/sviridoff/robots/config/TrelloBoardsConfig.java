package su.sviridoff.robots.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix="trello")
public class TrelloBoardsConfig {


    private Map<String, String> boards;

    @Bean
    public Map<String, String> boards() {
        return this.boards;
    }


}
