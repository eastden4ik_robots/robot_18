package su.sviridoff.robots.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix="trello")
public class TrelloListsConfig {


    private Map<String, String> lists;

    @Bean
    public Map<String, String> lists() {
        return this.lists;
    }


}
