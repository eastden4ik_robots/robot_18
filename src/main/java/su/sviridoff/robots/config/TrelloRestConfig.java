package su.sviridoff.robots.config;


import com.julienvey.trello.Trello;
import com.julienvey.trello.impl.TrelloImpl;
import com.julienvey.trello.impl.http.ApacheHttpClient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import su.sviridoff.robots.providers.SettingsProvider;

@Getter
@Configuration
@AllArgsConstructor
public class TrelloRestConfig {

    private final SettingsProvider provider;

    @Bean
    public Trello getTrelloImpl() {
        return new TrelloImpl(provider.getTrelloKey(), provider.getTrelloToken(), new ApacheHttpClient());
    }

}
