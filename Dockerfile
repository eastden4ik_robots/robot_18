FROM java:8

ADD ./target/robots-0.0.1-SNAPSHOT.jar robots-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/robots-0.0.1-SNAPSHOT.jar"]
